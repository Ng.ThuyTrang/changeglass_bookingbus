import React, { Component } from "react";

class DSgheDangDat extends Component {
  ShowGhe = () => {
    this.props.ChonGhe(this.props.data);
  };
  renderDsGhe = () => {
    return this.props.danhSachGheDaDat.map((item) => {
      return (
        <p className="mt-3 text-center">
          Ghế: {item.dsGhex.TenGhe}${item.dsGhex.Gia}
        </p>
      );
    });
  };
  render() {
    return (
      <div>
        <h3 className="mt-5 text-center" style={{ color: "#ffcc00" }}>
          Danh sách ghế đã đặt: {this.props.totalItem}
        </h3>
        {this.renderDsGhe()}
      </div>
    );
  }
}

export default DSgheDangDat;
